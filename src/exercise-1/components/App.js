import React, {Component} from 'react';
import '../styles/App.css';
import Home from "./Home/Home";
import Header from "./Header/Header";
import Profile from "./Profile/Profile";
import AboutUs from "./About/AboutUs";
import Product from "./Product/Product";
import ProductDetail from "./ProductDetail/ProductDetail";
import {Redirect, Route, Switch} from "react-router";
import {BrowserRouter as Router, HashRouter} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <Header/>
          <Switch>
            <Route exact path='/products' component={Product}/>
            <Route path='/my-profile' component={Profile}/>
            <Route path='/about-us' component={AboutUs}/>
            <Route exact path='/' component={Home}/>
            <Redirect from="/goods" to="/products"/>
            <Route path='/products/:id' component={ProductDetail}/>
            <Route component={Home}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
