import React from "react";
import {Link} from "react-router-dom";

class Header extends React.Component {
    render() {
        return (
            <ul>
                <li><Link to='/'>Home</Link></li>
                <li><Link to='/products'>Products</Link></li>
                <li><Link to='/my-profile'>My Profile</Link></li>
                <li><Link to='/about-us'>About Us</Link></li>
            </ul>
        );
    }
}

export default Header;
