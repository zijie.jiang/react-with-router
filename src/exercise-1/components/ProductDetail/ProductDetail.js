import React from "react";
import data from '../../../exercise-2/mockups/data';


const ProductDetail = (request) => {
    const url = request.location.pathname;
    const action = request.history.action;
    const id = parseInt(request.match.params.id);
    const productDetail = Object.values(data)[id - 1];
    console.log(productDetail);
    if (action === 'POP') {
        return (
            'Cannot go to product details page. Please go back to Products page'
        );
    } else {
        return (
            <ul>
                <li>Product Details:</li>
                <li>Name: {productDetail.name}</li>
                <li>Id: {productDetail.id}</li>
                <li>Price: {productDetail.price}</li>
                <li>Quantity: {productDetail.quantity}</li>
                <li>Desc: {productDetail.desc}</li>
                <li>URL: {url}</li>
            </ul>
        );
    }
};

export default ProductDetail;
