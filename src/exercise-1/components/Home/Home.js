import React from 'react';

const Home = () => {
    return (
        <p>
            This is a beautiful Home Page.<br/>
            And the url is '/'.
        </p>
    );
};

export default Home;
