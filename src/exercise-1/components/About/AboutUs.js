import React from 'react';
import {Link} from "react-router-dom";

const AboutUs = () => {
    return (
        <article>
            <p>
                Company: ThoughtWorks Local<br/>
                Location: Xi'an
            </p>
            <p>
                For more information, please<br/>
                view our <Link to='/'>website</Link>.
            </p>
        </article>
    );
};

export default AboutUs;
