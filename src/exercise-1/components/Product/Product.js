import data from '../../../exercise-2/mockups/data'
import React from "react";
import {Link} from "react-router-dom";

const Product = () => {
    return (
        <ul>
            <li>All Products</li>
            {
                Object.keys(data).map((item, index) => <li key={index}><Link to={`/products/${Object.values(data)[index].id}`}>{item}</Link></li>)
            }
        </ul>
    );
};

export default Product;
